@extends('template')

@section('content')
    <h1 class="page-header">Pedido: #{{ $pedido->id }}</h1>
        
    <p>
        <a href="{{ url('/pedido/editarPedido/' . $pedido->id) }}" class="btn btn-warning">Editar Pedido</a>
        <a href="{{ url('/') }}" class="btn btn-default">Listar Pedidos</a>
    </p>

    <p>
        <label for="cliente">Cliente:</label>
        {{ $pedido->clientes->nome }}
    </p>

    <label>Produtos do Pedido</label>

    <table class="table table-bordered produtos-pedido">
        <thead>
            <tr>
               <th>Código</th> 
               <th>Foto</th>
               <th>Nome</th>
               <th>Preço Unitario</th>
               <th>Preço do Item</th>
               <th>Quantidade</th>
               <th>Subtotal</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $quantidade = 0;
                $total = 0;
            ?>
            @foreach($pedido->itens as $i => $item)
                <tr>
                    <td>
                        {{ $item->produto->id }}
                    </td>
                    <td>
                        <div class="imagem-produto">
                            <img src="{{ $item->produto->imagem }}">
                        </div>
                    </td>
                    <td>
                        {{ $item->produto->nome }}
                    </td>
                    <td>
                        R$ {{ number_format($item->preco_unitario,2,',','.') }}
                    </td>
                    <td>
                        R$ <span class="preco-item">{{ number_format($item->preco_item,2,',','.') }}</span>
                    </td>
                    <td>
                        <span class="quantidade">{{ $item->quantidade }}</span>
                    </td>
                    <td>
                        R$ <span class="preco-subtotal">{{ number_format($item->preco_subtotal,2,',','.') }}</span>
                    </td>
                </tr>
                <?php
                    $quantidade += $item->quantidade;
                    $total += $item->preco_subtotal;
                ?>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="5"></th>
                <th><span id="quantidade-total">{{ $quantidade }}</span></th>
                <th>R$ <span id="preco-total">{{ number_format($total,2,',','.') }}</span></th>
            </tr>
        </tfoot>
    </table>

@endsection
