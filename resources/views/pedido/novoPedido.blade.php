@extends('template')

@section('content')
    <h1 class="page-header">Novo Pedido</h1>

    <p><a href="{{ url('/') }}" class="btn btn-default">Listar Pedidos</a></p>

    <form method="post" action="{{ url('pedido/salvarPedido') }}">

        {{ csrf_field() }}
        
        <div class="form-group">
            
            <label for="cliente">Cliente <small class="obrigatorio">(Obrigatório)</small></label>

            <select name="clientes_id" id="cliente" class="form-control" required="required">
                <option></option>
                @foreach($clientes as $cliente)
                    <option value="{{ $cliente->id }}">{{ $cliente->nome }}</option>
                @endforeach
            </select>

        </div>

        <div class="form-group">
            
            <label for="produto">Produto</label>

            <select name="" id="produto" class="form-control select-produto">
                <option></option>
                @foreach($produtos as $produto)
                    <option value="{{ $produto->id }}">{{ $produto->nome }}</option>
                @endforeach
            </select>

        </div>

        <div class="form-group">

        <label>Produtos do Pedido <small class="obrigatorio">(Obrigatório)</small></label>

            <table class="table table-bordered produtos-pedido">
                <thead>
                    <tr>
                       <th>Código</th> 
                       <th>Foto</th>
                       <th>Nome</th>
                       <th>Preço Unitario</th>
                       <th>Preço do Item</th>
                       <th>Quantidade</th>
                       <th>Subtotal</th>
                       <th width="5" colspan="2"></th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="5"></th>
                        <th><span id="quantidade-total">0</span></th>
                        <th colspan="3">R$ <span id="preco-total">0,00</span></th>
                    </tr>
                </tfoot>
            </table>

        </div>

        <button type="submit" class="btn btn-primary" id="verificar-pedido">Salvar Pedido</button>

    </form>

    <div class="modal fade" id="modal-produto" tabindex="-1" role="dialog" aria-labelledby="modal-produto">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Adicionar Produto</h4>
                </div>

                <div class="modal-body">

                    <div class="page-header">
                        <div class="row">
                            <div class="col-md-5">
                                <div id="modal-imagem-produto">
                                    <img src="">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <h3 id="modal-nome-produto">Produto</h3> <br>

                                <label for="preco-unitario">Preço Unitário</label>
                                <h4>R$ <span id="modal-preco-unitario">0,00</span></h4>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                         <div class="col-md-5">
                            <div class="form-group">
                                <label for="preco-item">Preço do Item</label>

                                <div class="input-group">
                                    <span class="input-group-addon" id="preco-item">R$</span>
                                    <input type="text" name="" class="form-control real" maxlength="13" id="modal-preco-item" aria-describedby="preco-item" value="0,00">
                                </div>
                                
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="aviso-rentabilidade"></div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="quantidade">Quantidade</label>
                                <input type="text" name="" class="form-control numerico" maxlength="6" id="modal-quantidade">
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="aviso-multiplo"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="preco-subtotal">Subtotal</label> <br>

                                <h4>R$ <span id="modal-preco-subtotal">0,00</span></h4>
                            </div>
                        </div>
                    </div>

                    <br style="clear:both;">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-success" id="adicionar-produto">Adicionar</button>
                </div>

            </div>
        </div>
    </div>

@endsection
