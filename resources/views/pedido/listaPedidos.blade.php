@extends('template')

@section('content')
    <h1 class="page-header">Pedidos</h1>

    <a href="{{ url('/pedido/novoPedido') }}" class="btn btn-default pull-right">Novo Pedido</a>

    <br><br><br>

    <table class="table table-condensed ">
        <thead>
            <tr>
                <th colspan="6">{{ $pedidos->count() }} registros encontrados</th>
            </tr>
            <tr>
                <th width="10%">Nº do Pedido</th>
                <th width="20%">Data e Hora</th>
                <th width="45%">Cliente</th>
                <th width="20%">Preço do Pedido</th>
                <th colspan="2" width="5%">Ações</th>
            </tr>
        </thead>

        <tbody>
            @if($pedidos->count())
                @foreach($pedidos as $pedido)
                    <tr>
                        <td>{{ $pedido->id }}</td>
                        <td>{{ $pedido->data_hora->format('d/m/Y H:i') }}</td>
                        <td>{{ $pedido->clientes->nome }}</td>
                        <td>R$ {{ number_format($pedido->preco_total,2,',','.') }}</td>
                        <td>
                            <a href="{{ url('/pedido/visualizarPedido/' . $pedido->id) }}" class="btn btn-info btn-xs">Visualizar</a>
                        </td>
                        <td>
                            <a href="{{ url('/pedido/editarPedido/' . $pedido->id) }}" class="btn btn-warning btn-xs">Editar</a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="6">Não houve pedidos realizados.</td>
                </tr>
            @endif
        </tbody>
    </table>
@endsection
