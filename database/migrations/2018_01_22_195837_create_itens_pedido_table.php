<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItensPedidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itens_pedido', function (Blueprint $table) {
            // $table->increments('id');
            $table->integer('produtos_id')->unsigned()->index();
            $table->integer('pedidos_clientes_id')->unsigned()->index();
            $table->integer('pedidos_id')->unsigned()->index();

            $table->decimal('preco_unitario', 10, 2);
            $table->decimal('preco_item', 10, 2);
            $table->integer('quantidade');
            $table->decimal('preco_subtotal', 10, 2);

            $table->foreign('produtos_id')->references('id')->on('produtos');
            $table->foreign('pedidos_id')->references('id')->on('pedidos');
            // $table->foreign('pedidos_clientes_id')->references('clientes_id')->on('pedidos');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itens_pedido');
    }
}
