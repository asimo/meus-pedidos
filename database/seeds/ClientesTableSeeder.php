<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clientes = [
            [ 	'id' => 1,
            	'nome' => 'Darth Vader'],
            [ 	'id' => 2,
            	'nome' => 'Obi-Wan Kenobi'],
           	[ 	'id' => 3,
            	'nome' => 'Luke Skywalker'],
            [ 	'id' => 4,
            	'nome' => 'Imperador Palpatine'],
            [ 	'id' => 5,
            	'nome' => 'Han Solo'],           
      	];

        foreach($clientes as $cliente):
            $this->command->info('Inserindo cliente: '. $cliente['nome']);
            DB::table('clientes')->insert($cliente);
        endforeach;
    }
}
