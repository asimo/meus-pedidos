<?php

use Illuminate\Database\Seeder;

class ProdutosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $produtos = [
            [ 	'id' => 1,
            	'nome' => 'Millenium Falcon',
            	'preco_unitario' => 550000.00,
            	'multiplo' => 0,
            	'imagem' => 'https://www.sideshowtoy.com/wp-content/uploads/2017/05/star-wars-millennium-falcon-die-cast-replica-efx-collectibles-silo-903033.png'],
            [ 	'id' => 2,
            	'nome' => 'X-Wing',
            	'preco_unitario' => 60000.00,
            	'multiplo' => 2,
            	'imagem' => 'https://vignette.wikia.nocookie.net/starwars/images/8/80/X-wing_Fathead.png/revision/latest/scale-to-width-down/500?cb=20161004003846'],
            [ 	'id' => 3,
            	'nome' => 'Super Star Destroyer',
            	'preco_unitario' => 4570000.00,
            	'multiplo' => 0,
            	'imagem' => 'https://nerdist.com/wp-content/uploads/2015/08/rsz_ssd-feature-08252015.jpg'],
            [ 	'id' => 4,
            	'nome' => 'TIE Fighter',
            	'preco_unitario' => 75000.00,
            	'multiplo' => 2,
            	'imagem' => 'https://www.wheelersluxurygifts.com/media/cache/sylius_gallery/ST-Dupont-Tie-Fighter-fountain-pen-with-cap-on-251683-747933c248e1d4d81796baaf078e29dd.jpeg'],
            [ 	'id' => 5,
            	'nome' => 'Lightsaber',
            	'preco_unitario' => 6000.00,
            	'multiplo' => 5,
            	'imagem' => 'https://www.stewiesplayground.com/grande/stewiesplayground/blue-luke-skywalker-lightsaber-superior-blue-light-saber-5-1488-x-2126.jpg'],
            [ 	'id' => 6,
            	'nome' => 'DLT-19 Heavy Blaster Rifle',
            	'preco_unitario' => 5800.00,
            	'multiplo' => 0,
            	'imagem' => 'https://vignette.wikia.nocookie.net/starwars/images/8/8e/DLT-19_DICE.png/revision/latest?cb=20161022005026'],
            [ 	'id' => 7,
            	'nome' => 'DL-44 Heavy Blaster Pistol',
            	'preco_unitario' => 1500.00,
            	'multiplo' => 10,
            	'imagem' => 'https://vignette.wikia.nocookie.net/starwars/images/8/83/DL-44_Blaster_Pistol_DICE.png/revision/latest?cb=20151106043425']
      	];

        foreach($produtos as $produto):
            $this->command->info('Inserindo produto: '. $produto['nome']);
            DB::table('produtos')->insert($produto);
        endforeach;
    }
}
