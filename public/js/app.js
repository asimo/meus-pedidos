$(document).ready(function(){

	$('.real').mask('000.000.000,00', {reverse: true});
	$('.numerico').mask('#####0');

	/*
	 Estiliza o select com bootstrap e adiciona um filtro de resultados
	 */
	$('select').selectpicker({
		liveSearch: true,
		noneSelectedText: 'Selecione...'
	});


	var precoItem 		= 0;
	var quantidade 		= 0;
	var subtotal 		= 0;
	// var quantidadeTotal = 0;
	// var precoTotal 		= 0;
	var idProduto 		= null;
	var produto 		= false;
	// var cacheProduto	= [];
	var cacheItem		= [];
	var item 			= $('.produtos-pedido .tbody tr').length;
	var idItem			= false;

	/*
	 Evento para abrir o modal com informações adicionais 
	 sobre o produto a ser inserido no pedido
	 */

	$(document).on('change', 'select.select-produto', function(){
		idProduto = $(this).val();
		$('#modal-produto').modal('show')
	});

	$(document).on('click', '.produtos-pedido .editar-item', function(){
		// idProduto = $(this).parents('tr').find('.produto-id').val();
		idItem = $(this).parents('tr').attr('item');

		if(cacheItem[idItem]){
			produto = cacheItem[idItem];
		}else{
			idProduto = $(this).parents('tr').find('.produto-id').val();
			produto = false;
		}

		$('#modal-produto').modal('show');
	});

	$('#modal-produto').on('show.bs.modal', function(event){
		var linhaProduto = null;
		var modal = $(this);

		modal.find('.modal-body').css('opacity', '0.1');

		if(!produto)
		{
			$.get('/pedido/dadosProdutoJson/' + idProduto, '', function(dadosProduto){
			    produto = jQuery.parseJSON(dadosProduto);

			    if(idItem !== false)
			    {
			    	var elemItem = $('tr[item='+idItem+']');
			    	produto.preco_item 	= elemItem.find('input.preco-item').val();
					produto.subtotal 	= elemItem.find('input.preco-subtotal').val();
					produto.quantidade 	= elemItem.find('input.quantidade').val();
					
					cacheItem[idItem] = produto;
			    }

			    gerarModal(modal, produto);
				verificarRentabilidade(produto.preco_unitario, produto.preco_unitario);
			});
		}
		else
		{
			gerarModal(modal, produto);
			verificarRentabilidade(produto.preco_item, produto.preco_unitario);
		}

	});

	$('#modal-produto').find('.modal-footer #adicionar-produto').on('click', function(){

		if(idItem)
		{
			cacheItem[idItem] = produto;
			cacheItem[idItem].preco_item 	= precoItem;
			cacheItem[idItem].subtotal 		= subtotal;
			cacheItem[idItem].quantidade 	= quantidade;

			editarItem(idItem);

		}
		else
		{
			cacheItem[item] = produto;
			cacheItem[item].preco_item 	= precoItem;
			cacheItem[item].subtotal 	= subtotal;
			cacheItem[item].quantidade 	= quantidade;

			incluirItem();
		}

		$('#modal-produto').modal('hide');

		recalcular();

	});

	function editarItem(idItem)
	{
		var itemProduto = $('.produtos-pedido tr[item='+idItem+']');
		itemProduto.find('span.preco-item').text(floatToReal(precoItem));
		itemProduto.find('input.preco-item').val(precoItem.toFixed(2));
		itemProduto.find('span.quantidade').text(quantidade);
		itemProduto.find('input.quantidade').val(quantidade);
		itemProduto.find('span.preco-subtotal').text(floatToReal(subtotal));
		itemProduto.find('input.preco-subtotal').val(subtotal.toFixed(2));
	}

	function incluirItem()
	{
		var linhaProduto = '<tr item="'+item+'">';
		linhaProduto += '<td>';
		linhaProduto += produto.id;
		linhaProduto += '<input type="hidden" name="produtos_id[]" class="produto-id" value="'+produto.id+'">';
		linhaProduto += '</td>';
		linhaProduto += '<td>';
		linhaProduto += '<div class="imagem-produto">';
		linhaProduto += '<img src="'+produto.imagem+'">';
		linhaProduto += '</div>';
		linhaProduto += '</td>';
		linhaProduto += '<td>';
		linhaProduto += produto.nome;
		linhaProduto += '</td>';
		linhaProduto += '<td>';
		linhaProduto += 'R$ ' + floatToReal(produto.preco_unitario);
		linhaProduto += '<input type="hidden" name="preco_unitario[]" class="preco-unitario" value="'+produto.preco_unitario+'">';
		linhaProduto += '</td>';
		linhaProduto += '<td>';
		linhaProduto += 'R$ <span class="preco-item">' + floatToReal(precoItem) + '</span>';
		linhaProduto += '<input type="hidden" name="preco_item[]" class="preco-item" value="'+precoItem.toFixed(2)+'">';
		linhaProduto += '</td>';
		linhaProduto += '<td>';
		linhaProduto += '<span class="quantidade">' + quantidade + '</span>';
		linhaProduto += '<input type="hidden" name="quantidade[]" class="quantidade" value="'+quantidade+'">';
		linhaProduto += '</td>';
		linhaProduto += '<td>';
		linhaProduto += 'R$ <span class="preco-subtotal">' + floatToReal(subtotal) + '</span>';
		linhaProduto += '<input type="hidden" name="preco_subtotal[]" class="preco-subtotal" value="'+subtotal.toFixed(2)+'">';
		linhaProduto += '</td>';
		linhaProduto += '<td>';
		linhaProduto += '<a href="#!" class="glyphicon glyphicon-remove remover-item"></a>';
		linhaProduto += '</td>';
		linhaProduto += '<td>';
		linhaProduto += '<a href="#!" class="glyphicon glyphicon-edit editar-item"></a>';
		linhaProduto += '</td>';
		linhaProduto += '</tr>';

		$('.produtos-pedido tbody').append(linhaProduto);

		item++;
	}

	$('#modal-produto').on('hide.bs.modal', function(event){
		var modal = $(this);

		modal.find('.modal-footer #adicionar-produto').attr('disabled', 'disabled');
		modal.find('.modal-body #modal-quantidade').val('');
		modal.find('.modal-body #modal-nome-produto').text('Produto');
		modal.find('.modal-body #modal-preco-unitario').text('0,00');
		modal.find('.modal-body #modal-preco-item').val('0,00');
		modal.find('.modal-body #modal-preco-subtotal').text('0,00');
		modal.find('.modal-body #modal-imagem-produto img').attr('src', '');
		modal.find('.aviso-multiplo').text('');
		modal.find('.aviso-rentabilidade').text('');
		modal.find('.modal-body .aviso-rentabilidade')
		   	 .removeClass('rentabilidade-boa')
		     .removeClass('rentabilidade-otima')
		     .removeClass('rentabilidade-ruim');

		$('select.select-produto').val('')
								  .selectpicker('val', '');

		produto = false;
		idItem	= false;

	});

	$('.produtos-pedido tbody').on('click', '.remover-item', function(){

		var linhaProduto = $(this).parents('tr');
		var idItem = linhaProduto.attr('item');

		// delete cacheItem[idItem];

		$(this).parents('tr').remove();

		recalcular();

	});

	$('#verificar-pedido').on('click',function(){
		$('.alerta-obrigatorio').remove();

		setTimeout(function(){
		    $('.alerta-obrigatorio').fadeOut(function(){
		    	$(this).remove();
		    });
		},8000);

		if(!$('#cliente').val())
		{
			$('#cliente').after('<h4 class="alerta-obrigatorio"><span class="label label-danger">Campo Obrigatório</span></h4>');
			return false;
		}

		if($('.produtos-pedido tbody tr').length == 0)
		{
			$('#produto').after('<h4 class="alerta-obrigatorio"><span class="label label-danger">Selecione produtos ao pedido</span></h4>');
			return false;
		}

	});

	function gerarModal(modal, produto)
	{
		modal.find('.modal-footer #adicionar-produto').attr('disabled', 'disabled');
		modal.find('.modal-body #modal-quantidade').val(produto.quantidade ? produto.quantidade : '');
		modal.find('.modal-body #modal-nome-produto').text(produto.nome);
		modal.find('.modal-body #modal-preco-unitario').text(floatToReal(produto.preco_unitario));
		modal.find('.modal-body #modal-preco-item').val(floatToReal(produto.preco_item ? produto.preco_item : produto.preco_unitario));
		modal.find('.modal-body #modal-preco-subtotal').text(floatToReal(produto.subtotal ? produto.subtotal : '0.00'));
		modal.find('.modal-body #modal-imagem-produto img').attr('src', produto.imagem);

		if(produto.multiplo)
		{
			modal.find('.modal-body .aviso-multiplo').text('*A quantidade para esse produto deve ser multiplos de '+ produto.multiplo);
		}
		else
		{
			modal.find('.modal-body .aviso-multiplo').text('');
		}

		verificarRentabilidade(produto.preco_unitario, produto.preco_unitario);

		modal.find('.modal-body #modal-quantidade, .modal-body #modal-preco-item').on('keyup', function(){
			
			precoItem 		= realToFloat(modal.find('.modal-body #modal-preco-item').val());
			quantidade 		= parseInt(modal.find('.modal-body #modal-quantidade').val());

			verificarRentabilidade(precoItem, produto.preco_unitario);

			validarQuantidade(quantidade, produto.multiplo, modal);

			subtotal = precoItem * quantidade;

			modal.find('.modal-body #modal-preco-subtotal').text(floatToReal(subtotal));

		});

		modal.find('.modal-body').css('opacity', '1');
	}

	function recalcular()
	{
		var quantidadeTotal = 0;
		var precoTotal = 0;

		$('.produtos-pedido tbody tr').each(function(){
			var quantidade = parseInt($(this).find('input.quantidade').val());
			var preco_subtotal = parseFloat($(this).find('input.preco-subtotal').val());
			
			quantidadeTotal += quantidade;
			precoTotal += preco_subtotal;
		});

		$('.produtos-pedido #quantidade-total').text(quantidadeTotal);
		$('.produtos-pedido #preco-total').text(floatToReal(precoTotal));

	}

	function verificarRentabilidade(precoItem, precoUnitario)
	{
		$('#modal-produto').find('.modal-body .aviso-rentabilidade')
						   .removeClass('rentabilidade-boa')
						   .removeClass('rentabilidade-otima')
						   .removeClass('rentabilidade-ruim')

		var porcento = 100 - ((precoItem * 100) / precoUnitario);

		if(porcento < 0)
		{
			$('#modal-produto').find('.modal-body .aviso-rentabilidade')
							   .addClass('rentabilidade-otima')
							   .text('Rentabilidade Ótima');
		}
		else if(porcento >= 0 && porcento <= 10)
		{
			$('#modal-produto').find('.modal-body .aviso-rentabilidade')
							   .addClass('rentabilidade-boa')
							   .text('Rentabilidade Boa');
		}
		else if(porcento > 10 || !porcento)
		{
			$('#modal-produto').find('.modal-body .aviso-rentabilidade')
							   .addClass('rentabilidade-ruim')
							   .text('Rentabilidade Ruim');
		} 
	}

	function verificarMultiplo(valor, multiplo)
	{
		if(multiplo == 0)
		{
			return false;
		}

		return valor % multiplo;
	}

	function validarQuantidade(quantidade, multiplo, elemModal)
	{
		elemModal.find('.modal-body #modal-quantidade').removeClass('error');
		elemModal.find('.modal-footer #adicionar-produto').removeAttr('disabled');
		
		if(verificarMultiplo(quantidade, multiplo) || !quantidade)
		{
			elemModal.find('.modal-body #modal-quantidade').addClass('error');
			elemModal.find('.modal-footer #adicionar-produto').attr('disabled', 'disabled');
		}
	}

	/*
	 Fonte: http://blog.fmansano.com/javascript/converter-numero-para-moeda-e-vice-versa/
	 */
	function floatToReal(n, c, d, t)
	{
	    c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
	    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	}

	function realToFloat(valor)
	{
	    return isNaN(valor) == false ? parseFloat(valor) :   parseFloat(valor.replace(".","").replace(".","").replace(",","."));
	}
});
