<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
	private $_fillable = ['produtos_id', 'pedidos_clientes_id', 'pedidos_id', 'preco_unitario', 'preco_item', 'quantidade', 'preco_subtotal'];
	public $timestamps = false;
}
