<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemPedido extends Model
{
    public $timestamps = false;
    public $table = 'itens_pedido';

    public function produto()
    {
        return $this->belongsTo('App\Produto', 'produtos_id', 'id');
    }
}
