<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Pedido extends Model
{

	protected $fillable = ['clientes_id', 'data_hora', 'preco_total', 'quantidade_total'];
	public $timestamps = false;
	protected $dates = ['data_hora'];

	// public function 

	public function salvarPedido($dados)
	{
		$pedido = self::create([
					'clientes_id' => $dados['clientes_id'], 
					'data_hora'   => date('Y-m-d H:i:s'),
					'preco_total' => array_sum($dados['preco_subtotal']),
					'quantidade_total' => array_sum($dados['quantidade'])]);

		$itens = $this->_organizarItens($dados);

		foreach($dados['produtos_id'] as $chave => $produto_id){
			$pedido->itensPedido()->attach($produto_id, $itens[$chave]);
		}
	}
	
	public function alterarPedido($id, $dados)
	{
		ItemPedido::where('pedidos_id', $id)->delete();

		$pedido = self::find($id);
		$pedido->update([
					'clientes_id' => $dados['clientes_id'], 
					'data_hora'   => date('Y-m-d H:i:s'),
					'preco_total' => array_sum($dados['preco_subtotal']),
					'quantidade_total' => array_sum($dados['quantidade'])]);

		$itens = $this->_organizarItens($dados);

		foreach($dados['produtos_id'] as $chave => $produto_id){
			$pedido->itensPedido()->attach($produto_id, $itens[$chave]);
		}
	}

    public function listaClientes()
    {
    	return Cliente::all();
    }

    public function listaProdutos()
    {
    	return Produto::all();
    }

    public function dadosProduto(int $id)
    {
    	return Produto::find($id);
    }

    public function itensPedido()
    {
        return $this->belongsToMany('App\Produto', 'itens_pedido', 'pedidos_id', 'produtos_id');
    }

    public function itens()
    {
        return $this->hasMany('App\ItemPedido', 'pedidos_id');
    }

    function clientes()
	{
	    return $this->belongsTo('App\Cliente');
	}

	// function 

    private function _organizarItens($post)
    {
    	$itens = [];

    	$dados = $post;
    	unset($dados['_token']);
    	unset($dados['clientes_id']);
    	unset($dados['produto']);
    	unset($dados['produtos_id']);
    	unset($dados['_method']);

    	foreach($dados as $coluna => $valores){

    		foreach($valores as $chave => $valor){
    			if(!isset($itens[$chave]['pedidos_clientes_id'])){
    				$itens[$chave]['pedidos_clientes_id'] = $post['clientes_id'];
    			}

    			$itens[$chave][$coluna] = $valor;
    		}
    	}

    	return $itens;
    }
}
