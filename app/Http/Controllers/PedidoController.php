<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pedido;
use App\Http\Requests\PedidoRequest;

class PedidoController extends Controller
{

	private $pedidoModel = null;

	public function __construct()
	{
		$this->pedidoModel = new Pedido;
	}
    
	public function listaPedidos()
	{
		return view('pedido.listaPedidos', ['pedidos' => $this->pedidoModel->all()]);
	}

	public function novoPedido()
	{
		return view('pedido.novoPedido', ['clientes' => $this->pedidoModel->listaClientes(), 'produtos' => $this->pedidoModel->listaProdutos()]);
	}

	public function salvarPedido(PedidoRequest $request)
	{

		$this->pedidoModel->salvarPedido($request->all());

		return redirect('/');
	}

	public function editarPedido(int $id)
	{
		return view('pedido.editarPedido', ['pedido' => $this->pedidoModel->find($id), 'clientes' => $this->pedidoModel->listaClientes(), 'produtos' => $this->pedidoModel->listaProdutos()]);
	}

	public function alterarPedido(int $id, PedidoRequest $request)
	{
		$this->pedidoModel->alterarPedido($id, $request->all());

		return redirect('/pedido/editarPedido/' . $id);
	}

	public function visualizarPedido(int $id)
	{
		return view('pedido.visualizarPedido', ['pedido' => $this->pedidoModel->find($id)]);
	}

	public function dadosProdutoJson($id)
	{
		echo json_encode($this->pedidoModel->dadosProduto($id));
	}

}
