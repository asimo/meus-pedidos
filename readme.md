## Meus Pedidos

[https://solucao-meus-pedidos.herokuapp.com](https://solucao-meus-pedidos.herokuapp.com)

Tecnologias utilizadas

- PHP 7.1
- Laravel 5.5
- jQuery 3.3
- [jQuery Mask Plugin](https://igorescobar.github.io/jQuery-Mask-Plugin)
- [botstrap-select](https://silviomoreto.github.io/bootstrap-select)
- [Bootstrap 3](http://getbootstrap.com)
- PostgreSQL 10.1