<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PedidoController@listaPedidos');
Route::get('/pedido/novoPedido', 'PedidoController@novoPedido');
Route::post('/pedido/salvarPedido', 'PedidoController@salvarPedido');
Route::get('/pedido/dadosProdutoJson/{id}', 'PedidoController@dadosProdutoJson');
Route::get('/pedido/editarPedido/{id}', 'PedidoController@editarPedido');
Route::put('/pedido/alterarPedido/{id}', 'PedidoController@alterarPedido');
Route::get('/pedido/visualizarPedido/{id}', 'PedidoController@visualizarPedido');
